//
//  ViewController.swift
//  ProjectC
//
//  Created by Oscar Perdanakusuma on 30/01/2018.
//  Copyright © 2018 Oscar Perdanakusuma. All rights reserved.
//

import UIKit

public class OtherViewController: UIViewController {

    public static func create() -> OtherViewController {
        let bundle = Bundle(for: OtherViewController.self)
        let board = UIStoryboard(name: "ProjectCMain", bundle: bundle)
        let controller = board.instantiateInitialViewController() as! OtherViewController
        return controller
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        let m = ModelMapper()
        m.example()
    }


}

