//
//  main.m
//  MainProject
//
//  Created by Oscar Perdanakusuma on 30/01/2018.
//  Copyright © 2018 Oscar Perdanakusuma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
