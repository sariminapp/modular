//
//  ViewController.h
//  ProjectA
//
//  Created by Oscar Perdanakusuma on 30/01/2018.
//  Copyright © 2018 Oscar Perdanakusuma. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyViewController : UIViewController

+ (instancetype)create;

@end

NS_ASSUME_NONNULL_END
